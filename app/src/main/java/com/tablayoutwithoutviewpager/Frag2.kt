package com.tablayoutwithoutviewpager

import android.app.Fragment
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by yudizsolutionspvtltd on 15/02/17.
 */

class Frag2 : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.frag2_layout, container, false)

        return view

    }
}
