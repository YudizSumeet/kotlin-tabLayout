package com.tablayoutwithoutviewpager

import android.app.Fragment
import android.app.FragmentTransaction
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var fragmentTransaction: FragmentTransaction? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        addFirstFrag()
        designTabLayout()

        actMain_tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                setCurrentTabFragment(actMain_tabLayout!!.getSelectedTabPosition())

            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }


        })

    }


    internal fun addFirstFrag() {
        fragmentTransaction = fragmentManager!!.beginTransaction()

        fragmentTransaction!!.replace(R.id.actMain_frame, Frag1())
        fragmentTransaction!!.addToBackStack("")
        fragmentTransaction!!.commit()

    }

    internal fun designTabLayout() {

        actMain_tabLayout!!.addTab(actMain_tabLayout!!.newTab().setIcon(R.mipmap.ic_launcher).setText("1"))
        actMain_tabLayout!!.addTab(actMain_tabLayout!!.newTab().setIcon(R.mipmap.ic_launcher).setText("2"))
        actMain_tabLayout!!.addTab(actMain_tabLayout!!.newTab().setIcon(R.mipmap.ic_launcher).setText("3"))

    }

    internal fun addFrag(fragment: Fragment) {
        fragmentTransaction = fragmentManager!!.beginTransaction()

        fragmentTransaction!!.replace(R.id.actMain_frame, fragment)
        fragmentTransaction!!.addToBackStack("")
        fragmentTransaction!!.commit()

    }

    internal fun setCurrentTabFragment(pos: Int) {

        when (pos) {

            0 ->

                addFrag(Frag1())

            1 ->

                addFrag(Frag2())

            2 ->

                addFrag(Frag3())
        }

    }


}

